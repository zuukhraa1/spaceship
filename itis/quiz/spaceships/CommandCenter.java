package itis.quiz.spaceships;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).

import java.util.ArrayList;

public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        if (ships != null && ships.size() > 0) {
            Spaceship mostPowerfulShip = ships.get(0);
            for (int i = 1; i < ships.size(); i++) {
                if (mostPowerfulShip.getFirePower() < ships.get(i).getFirePower()) {
                    mostPowerfulShip = ships.get(i);
                }
            }
            if (mostPowerfulShip.getFirePower() != 0) {
                return mostPowerfulShip;
            }
        }
        return null;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        if (ships != null) {
            for (int i = 0; i < ships.size(); i++) {
                if (name.equals(ships.get(i).getName())) {
                    return ships.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        if (ships != null) {
            for (int i = 0; i < ships.size(); i++) {
                if (ships.get(i).getCargoSpace() >= cargoSize) {
                    shipsWithEnoughCargoSpace.add(ships.get(i));
                }
            }
        }
        return shipsWithEnoughCargoSpace;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civilianShips = new ArrayList<>();
        if (ships != null) {
            for (int i = 0; i < ships.size(); i++) {
                if (ships.get(i).getFirePower() <= 0) {
                    civilianShips.add(ships.get(i));
                }
            }
        }
        return civilianShips;
    }
}
