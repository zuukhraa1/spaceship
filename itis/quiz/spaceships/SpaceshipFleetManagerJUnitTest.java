package itis.quiz.spaceships;

import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class SpaceshipFleetManagerJUnitTest {

    private SpaceshipFleetManager center;
    ArrayList<Spaceship> testList;

    @BeforeEach
    public void createTestShips() {
        center = new CommandCenter();
        testList = new ArrayList<>();
        testList.add(new Spaceship("Kevin", 155, 15, 10));
        testList.add(new Spaceship("Veronica", 100, 20, 5));
        testList.add(new Spaceship("Ben", 95, 5, 15));
    }

    @Test
    @DisplayName("Возвращает конкретный корабль из списка кораблей с заданным именем")
    public void getShipByName_shipExists_returnTargetShip() {
        String testName = "Veronica";
        Spaceship result = center.getShipByName(testList, testName);
        Assertions.assertEquals(result.getName(), testName);
    }

    @Test
    @DisplayName("Возвращает null, так как коробля с заданным именем нет в списке кораблей")
    public void getShipByName_shipNoExists_returnNull() {
        String testName = "Yana";
        Spaceship result = center.getShipByName(testList, testName);
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Возвращает самый сильный корабль")
    public void getMostPowerfulShip_shipInAnyPosition_returnTargetShip() {
        testList.add(new Spaceship("Ann", 200, 15, 10));
        testList.add(new Spaceship("Carl", 180, 15, 10));
        int testFirePower = 200;
        Spaceship result = center.getMostPowerfulShip(testList);
        Assertions.assertEquals(result.getFirePower(), testFirePower);
    }

    @Test
    @DisplayName("Возвращает самый сильный корабль первый по списку")
    public void getMostPowerfulShip_shipInFirstPosition_returnTargetShip() {
        Spaceship result = center.getMostPowerfulShip(testList);
        Assertions.assertEquals(result, testList.get(0));
    }

    @Test
    @DisplayName("Возвращает null, так как все корабли мирные")
    public void getMostPowerfulShip_shipNoExists_returnNull() {
        testList = new ArrayList<>();
        testList.add(new Spaceship("Kevin", 0, 15, 10));
        testList.add(new Spaceship("Veronica", 0, 20, 5));
        testList.add(new Spaceship("Ben", 0, 5, 15));
        Spaceship result = center.getMostPowerfulShip(testList);
        Assertions.assertNull(result);
    }

    @Test
    @DisplayName("Возвращает список кораблей, в котором хотя бы 1 корабль мирный")
    public void getAllCivilianShips_shipsExist_returnListOfShips() {
        testList.add(new Spaceship("Ann", 0, 15, 10));
        testList.add(new Spaceship("Carl", 180, 15, 10));
        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        Assertions.assertTrue(result.size() > 0);
    }

    @Test
    @DisplayName("Возвращает пустой список, так как все корабли не мирные")
    public void getAllCivilianShips_shipsNoExist_returnEmptyListOfShips() {
        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);

    }

    @Test
    @DisplayName("Возвращает список кораблей, в котором хотя бы 1 корабль имеет достаточно места в грузовом отсеке")
    public void getAllShipsWithEnoughCargoSpace_shipsExist_returnListOfShips() {
        int testCargoSpace = 19;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);
        Assertions.assertTrue(result.size() > 0);
    }

    @Test
    @DisplayName("Возвращает пустой список, так как все корабли не имеют достаточно места в грузовом отсеке")
    public void getAllShipsWithEnoughCargoSpace_shipsNoExist_returnEmptyListOfShips() {
        int testCargoSpace = 21;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);
        Assertions.assertEquals(result.size(), 0);
    }

}
