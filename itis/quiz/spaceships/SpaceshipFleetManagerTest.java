package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {

    SpaceshipFleetManager center;

    public static void main(String[] args) {

        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());

        boolean test1 = spaceshipFleetManagerTest.getShipByName_shipExists_returnTargetShip();
        boolean test2 = spaceshipFleetManagerTest.getShipByName_shipNoExists_returnNull();
        boolean test3 = spaceshipFleetManagerTest.getMostPowerfulShip_shipInAnyPosition_returnTargetShip();
        boolean test4 = spaceshipFleetManagerTest.getMostPowerfulShip_shipInFirstPosition_returnTargetShip();
        boolean test5 = spaceshipFleetManagerTest.getMostPowerfulShip_shipNoExists_returnNull();
        boolean test6 = spaceshipFleetManagerTest.getAllCivilianShips_shipsExist_returnListOfShips();
        boolean test7 = spaceshipFleetManagerTest.getAllCivilianShips_shipsNoExist_returnEmptyListOfShips();
        boolean test8 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipsExist_returnListOfShips();
        boolean test9 = spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_shipsNoExist_returnEmptyListOfShips();

        if (test1) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 1 failed!");
        }

        if (test2) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 2 failed!");
        }

        if (test3) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 3 failed!");
        }

        if (test4) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 4 failed!");
        }

        if (test5) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 5 failed!");
        }

        if (test6) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 6 failed!");
        }

        if (test7) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 7 failed!");
        }

        if (test8) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 8 failed!");
        }

        if (test9) {
            System.out.println("All good!");
        } else {
            System.out.println("Test 9 failed!");
        }

    }

    public SpaceshipFleetManagerTest(SpaceshipFleetManager center) {
        this.center = center;
    }

    // метод, чтобы не дублировать код в тестах
    private ArrayList<Spaceship> createTestShips() {
        ArrayList<Spaceship> spaceships = new ArrayList<>();
        spaceships.add(new Spaceship("Kevin", 155, 15, 10));
        spaceships.add(new Spaceship("Veronica", 100, 20, 5));
        spaceships.add(new Spaceship("Ben", 95, 5, 15));
        return spaceships;
    }

    // test1
    // возвращает конкретный корабль из списка кораблей с заданным именем
    private boolean getShipByName_shipExists_returnTargetShip() {
        ArrayList<Spaceship> testList = createTestShips();
        String testName = "Veronica";
        Spaceship result = center.getShipByName(testList, testName);
        return result != null && result.getName().equals(testName);
    }

    // test2
    // возвращает null, так как коробля с заданным именем нет в списке кораблей
    private boolean getShipByName_shipNoExists_returnNull() {
        ArrayList<Spaceship> testList = createTestShips();
        String testName = "Yana";
        Spaceship result = center.getShipByName(testList, testName);
        return result == null;
    }

    // test3
    // возврашает самый сильный корабль
    private boolean getMostPowerfulShip_shipInAnyPosition_returnTargetShip() {
        ArrayList<Spaceship> testList = createTestShips();
        testList.add(new Spaceship("Ann", 200, 15, 10));
        testList.add(new Spaceship("Carl", 180, 15, 10));
        int testFirePower = 200;
        Spaceship result = center.getMostPowerfulShip(testList);
        return result.getFirePower() == testFirePower;
    }

    // test4
    // возвращает самый сильный корабль первый по списку
    private boolean getMostPowerfulShip_shipInFirstPosition_returnTargetShip() {
        ArrayList<Spaceship> testList = createTestShips();
        Spaceship result = center.getMostPowerfulShip(testList);
        return result.equals(testList.get(0));
    }

    // test5
    // возвращает null, так как все корабли мирные
    private boolean getMostPowerfulShip_shipNoExists_returnNull() {
        ArrayList<Spaceship> testList = new ArrayList<>();
        testList.add(new Spaceship("Kevin", 0, 15, 10));
        testList.add(new Spaceship("Veronica", 0, 20, 5));
        testList.add(new Spaceship("Ben", 0, 5, 15));
        Spaceship result = center.getMostPowerfulShip(testList);
        return result == null;
    }

    // test6
    // возвращает список кораблей, в котором хотя бы 1 корабль мирный
    private boolean getAllCivilianShips_shipsExist_returnListOfShips() {
        ArrayList<Spaceship> testList = createTestShips();
        testList.add(new Spaceship("Ann", 0, 15, 10));
        testList.add(new Spaceship("Carl", 180, 15, 10));
        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        return result.size() > 0;
    }

    // test7
    // возвращает пустой список, так как все корабли не мирные
    private boolean getAllCivilianShips_shipsNoExist_returnEmptyListOfShips() {
        ArrayList<Spaceship> testList = createTestShips();
        ArrayList<Spaceship> result = center.getAllCivilianShips(testList);
        return result.size() == 0;
    }

    // test8
    // возвращает список кораблей, в котором хотя бы 1 корабль имеет достаточно места в грузовом отсеке
    private boolean getAllShipsWithEnoughCargoSpace_shipsExist_returnListOfShips() {
        ArrayList<Spaceship> testList = createTestShips();
        int testCargoSpace = 19;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);
        return result.size() > 0;
    }

    // test9
    // возвразает пустой список, так как все корабли не имеют достаточно места в грузовом отсеке
    private boolean getAllShipsWithEnoughCargoSpace_shipsNoExist_returnEmptyListOfShips() {
        ArrayList<Spaceship> testList = createTestShips();
        int testCargoSpace = 21;
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(testList, testCargoSpace);
        return result.size() == 0;
    }

}
